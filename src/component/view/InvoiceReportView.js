import { Show, TabbedShowLayout } from "react-admin";
import { CommonView } from "./CommonView";

export const InvoiceReportView = () => (
    <Show>
        <TabbedShowLayout>
            <TabbedShowLayout.Tab label="General">
                <CommonView
                    fields={["invoiceNumber"]}
                    isView={true}
                    labels={["Invoice Number"]}
                />
            </TabbedShowLayout.Tab>
        </TabbedShowLayout>
    </Show>
);
