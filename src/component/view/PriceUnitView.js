import { Show, TabbedShowLayout } from "react-admin";
import { CommonView } from "./CommonView";

export const PriceUnitView = () => (
  <Show>
    <TabbedShowLayout>
      <TabbedShowLayout.Tab label="General">
        <CommonView
          fields={["name", "nameAr", "createdAt", "updatedAt"]}
          isView={true}
          labels={["Name", "Arabic Name", "Created At", "Updated At"]}
        />
      </TabbedShowLayout.Tab>
    </TabbedShowLayout>
  </Show>
);
