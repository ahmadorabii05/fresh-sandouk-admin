import { Create } from "react-admin";

export const CreateView = ({ resource, children }) => {
  return <Create title={`Create ${resource}`}>{children}</Create>;
};
