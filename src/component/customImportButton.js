import { red } from '@mui/material/colors';
import React, { useCallback } from 'react';
import { Button } from 'react-admin';
import { useDropzone } from 'react-dropzone';
import { utils, read } from 'xlsx';

const CustomImportButton = ({ onImport, label }) => {
    const onDrop = useCallback((acceptedFiles) => {
        acceptedFiles.forEach((file) => {
            const reader = new FileReader();
            reader.onload = (event) => {
                const fileContent = event.target.result;

                // Parse the Excel file
                const workbook = read(fileContent, { type: 'binary' });

                // Create an array to store sheet data
                const sheetsData = [];

                workbook.SheetNames.forEach((sheetName) => {
                    const sheetData = utils.sheet_to_json(workbook.Sheets[sheetName]);
                    sheetsData.push({ sheetName, data: sheetData });
                });

                // Pass the array of sheet data to the onImport callback
                onImport(sheetsData);
            };
            reader.readAsBinaryString(file);
        });
    }, [onImport]);

    const { getRootProps, getInputProps } = useDropzone({ onDrop });

    return (
        <div {...getRootProps()}>
            <input {...getInputProps()} />
            <Button style={{ backgroundColor: 'rgb(33,150,243)', color: 'white', width: 200 }} label={label} />
        </div>
    );
};

export default CustomImportButton;
