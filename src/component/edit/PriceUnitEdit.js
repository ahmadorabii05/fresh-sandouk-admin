import { Edit, SimpleForm, TabbedShowLayout } from "react-admin";
import { CommonEdit } from "./CommonEdit";

export const PriceUnitEdit = () => (
  <Edit>
    <SimpleForm>
      <TabbedShowLayout>
        <TabbedShowLayout.Tab label="General">
          <CommonEdit fields={["name", "nameAr"]} />
        </TabbedShowLayout.Tab>
      </TabbedShowLayout>
    </SimpleForm>
  </Edit>
);
