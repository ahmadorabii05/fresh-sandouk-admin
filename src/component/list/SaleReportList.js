import { List } from "react-admin";
import { CommonList } from "./CommonList";
export const SaleReportList = (props) => (
  <List {...props} title="Sales Report">
    <CommonList
      resource={"sales-report"}
      fields={[
        "name",
        "sku",
        "createdAt",
        "updatedAt",
        "category",
        "brand",
        "quantity",
        "quantitySold",
        "totalSoldAmount",
      ]}
      includeBulkActions={true}
    />
  </List>
);
