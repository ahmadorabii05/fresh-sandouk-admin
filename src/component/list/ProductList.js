import {
    CreateButton,
    downloadCSV,
    ExportButton,
    FilterButton,
    List,
    Pagination,
    TextInput,
    TopToolbar,
    useListContext,
    useNotify
} from "react-admin";
import {CommonList} from "./CommonList";
import CustomImportButton from "../customImportButton";
import ProductDataProvider from "../../dataProviders/productDataProvider";
import jsonExport from 'jsonexport/dist';

const filters = [
    <TextInput label="Title" source="name" defaultValue=""/>,

];

const exporter = posts => {

    const postsForExport = posts.map(post => {
        return {
            "Barcode": post.barcode,
            "Name": post.name,
            "Description": post.description,
            "Brand": post.brand,
            "Sub Categories": post.subCategories,
            "Unit": post.unit,
            "Price": post.price,
            "Promotion Price": `AED ${post.finalPriceAfterDiscount}`,
            "Is Active": post.isActive,
            "Is Featured": post.isFeatured
        };
    });
    jsonExport(postsForExport, {
        headers: ['Barcode', 'Name', 'Description', 'Brand', 'Sub Categories', 'Unit', 'Price', 'Promotion Price', 'Is Active','Is Featured'] // order fields in the export
    }, (err, csv) => {
        downloadCSV(csv, 'products'); // download as 'products.csv` file
    });
};

const PostPagination = ({total}) => <Pagination total={total} rowsPerPageOptions={[10, 25, 50, 100]}/>;
const ListActions = ({handleImport, handleUpdateBulkImport}) => (
    <TopToolbar>
        <CustomImportButton label="Create Products" onImport={handleImport}/>
        <CustomImportButton label="Update Prices" onImport={handleUpdateBulkImport}/>
        <FilterButton/>
        <CreateButton/>
        <ExportButton maxResults={10000} exporter={exporter}/>
    </TopToolbar>
);
export const ProductList = (props) => {
    const notify = useNotify();
    const handleImport = async (parsedData) => {
        const resource = 'products';
        const params = {data: parsedData};
        try {
            await ProductDataProvider.createMany(resource, params);
            notify(`Products added Successfully.`, {type: "success"});
        } catch (error) {
            notify(`${error}`, {type: "error"});
        }
    };

    const handleUpdateBulkImport = async (parsedData) => {
        const resource = 'products';
        const params = {data: parsedData};
        try {
            await ProductDataProvider.updateMany(resource, params);
            notify(`Prices updated successfully.`, {type: "success"});
        } catch (error) {
            notify(`${error.message}`, {type: "error"});
        }
    };


    const {total} = useListContext();
    return (
        <>
`            <List actions={<ListActions handleImport={handleImport} handleUpdateBulkImport={handleUpdateBulkImport}/>}
                  filters={filters} {...props} pagination={<PostPagination total={total}/>} title="Product List">
                <CommonList
                    fields={["image", "name", "country", "price", "quantity", "isActive", "isFeatured", "createdAt"]}
                    includeBulkActions={false} includeDelete={true}/>
            </List>
        </>
    )
};
