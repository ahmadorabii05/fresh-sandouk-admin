import { List } from "react-admin";
import { CommonList } from "./CommonList";
export const InventoryReportList = (props) => (
  <List {...props} title="Inventory Report">
    <CommonList
      resource={"inventory-report"}
      fields={[
        "name",
        "sku",
        "createdAt",
        "updatedAt",
        "category",
        "brand",
        "price",
        "unit",
        "quantity",
      ]}
      includeBulkActions={false}
    />
  </List>
);
