import { List } from "react-admin";
import { CommonList } from "./CommonList";
export const BannerList = (props) => (
  <List {...props} title="Banner List">
    <CommonList fields={["image", "location", "createdAt", "updatedAt"]} includeBulkActions={true} />
  </List>
);
