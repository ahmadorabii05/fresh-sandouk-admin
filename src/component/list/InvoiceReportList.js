import { List } from "react-admin";
import { CommonList } from "./CommonList";
export const InvoiceReportList = (props) => (
  <List {...props} title="Invoice Report">
    <CommonList
      resource={"invoice-report"}
      fields={["invoiceNumber", "customerName", "totalAmount"]}
      includeBulkActions={false}
    />
  </List>
);
