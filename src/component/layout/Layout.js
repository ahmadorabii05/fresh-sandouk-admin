import { Layout } from "react-admin";

import { SideMenu } from "../sideMenu/SideMenu";

export const MyLayout = (props) => <Layout {...props} menu={SideMenu} />;
