import { CreateView } from "../createView";
import { CommonForm } from "./CommonForm";

export const TagForm = (props) => {
  return (
    <CreateView resource="tag">
      <CommonForm
        fields={["name", "nameAr", "image"]}
        includeSimpleForm={true}
        {...props}
      />
    </CreateView>
  );
};
