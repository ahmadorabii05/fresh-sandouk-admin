export default class NumberFormatter {
    static instance = new NumberFormatter();

    // example of options would be { style: 'currency', currency: 'USD' }
    // return new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD'}).format();
    formatNumber(options, amount) {
        return new Intl.NumberFormat('en-US', { ...options }).format(amount);
    }
}