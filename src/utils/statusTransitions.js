// statusTransitions.js

export const Order = {
    STATUS_CONFIRMED: 1,
    STATUS_PROCESSING: 2,
    STATUS_OUT_OF_DELIVERY: 3,
    STATUS_DELIVERED: 4,
    STATUS_CLOSED: 5,
    STATUS_CANCEL: 6,

    ALLOWED_TRANSITIONS: {
        1: [2, 3, 4, 5, 6], // STATUS_CONFIRMED
        2: [3, 4, 5, 6],    // STATUS_PROCESSING
        3: [4, 5, 6],       // STATUS_OUT_OF_DELIVERY
        4: [5, 6],          // STATUS_DELIVERED
        5: [5],             // STATUS_CLOSED
        6: []               // STATUS_CANCEL
    }
};

export function changeOrderStatus(currentStatus, newStatus) {
    return Order.ALLOWED_TRANSITIONS[currentStatus]?.includes(newStatus);
}
